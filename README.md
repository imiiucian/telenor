## Requirements: 
    php7.2 and composer

## Instructions:

    1. Clone the master branch and install dependencies with composer install
    2. Add the following cron job in the server for AUTOMATIC DELETION of EXPIRED ENTRIES
        * * * * * cd /path-to-the-project && php artisan schedule:run >> /dev/null 2>&1
        
## Available Routes:

    1. GET api/values (Get all values or specific value with query string. All get requests reset the TTL as suggested)

    2. POST api/values (Refer to Example session for post body)
    
    3. PATCH api/values (Refer to Example session for post body)

## Examples:

#### POST 
    POST /api/values HTTP/1.1
    Host: 127.0.0.1:8000
    Content-Type: application/json
    Accept: application/json

    {
        "key4": "val4",
        "key2": "val2",
        "key5": "val5"
    }


### PATCH
    PATCH /api/values HTTP/1.1
    Host: 127.0.0.1:8000
    Content-Type: application/json
    Accept: application/json

    {
        "key46": "val45",
        "key25": "val69",
        "key101": "val693"
    }

### GET all

    GET /api/values HTTP/1.1
    Host: 127.0.0.1:8000
    Content-Type: application/json
    Accept: application/json

### GET with query string

    GET /api/values?keys=key2,key6 HTTP/1.1
    Host: 127.0.0.1:8000
    Content-Type: application/json
    Accept: application/json

#### N.B : A json file of postman collection is attached named "telenor.postman_collection.json" with this repo 
