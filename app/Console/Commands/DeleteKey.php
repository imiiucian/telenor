<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class DeleteKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:keys';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete expired keys';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $keys = json_decode(Cache::get('keys'));

        if(!is_array($keys)){
            return;
        }
        logger('Deleting Keys');
        foreach ($keys as $key => $value) {
            Cache::forget($key);
            unset($keys[$key]);
        }

        Cache::put('keys', json_encode(array_values($keys)));

    }
}
