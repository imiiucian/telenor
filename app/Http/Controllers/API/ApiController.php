<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('keys'))
        {
            return $this->show($request);
        }

        $keys = json_decode(Cache::get('keys'));

        $response = [];

        $ttl = intval(env('CACHE_EXPIRATION', 300));        

        if(!is_array($keys))
        {
            $keys = [];
        }

        foreach ($keys as $key) {
            $value = Cache::get($key);
           
            if($value != null){
                Cache::put($key, $value, $ttl); // reset ttl values as of suggestion
                $response[$key] = $value;                
            }
        }

        return response()->json($response, count($response) > 0 ? 200 : 404);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vars = $request->all();

        $coun = 0;

        $ttl = intval(env('CACHE_EXPIRATION', 300));

        $all_keys = json_decode(Cache::get('keys'));
        if(!is_array($all_keys))
        {
            $all_keys = [];
        }

        foreach ($vars as $key => $value)
        {
            $added = Cache::add($key, $value, $ttl);

            if($added === true)
            {
                if(in_array($key, $all_keys) == false){
                    array_push($all_keys, $key);
                }
            } else {
                $coun++;
            }   
        }

        Cache::put('keys', json_encode($all_keys));

        $messgae = $coun == 0 ? 'All values are stored successfully' : $coun . ' value(s) could not be stored';

        return response()->json([
            'message' => $messgae,
        ], 201);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $keys = $request->keys;

        $keys = explode(',', $keys);

        $ttl = intval(env('CACHE_EXPIRATION', 300));        

        $response = [];

        foreach ($keys as $key) {
            $value = Cache::get($key);
            if($value != null){
                Cache::put($key, $value, $ttl);
                $response[$key] = $value;
            }
        }

        if(count($response) > 0){
            return response()->json($response, 200);
        } else {
            return response()->json(['message' => 'Not Found!'], 404);            
        }
        

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $vars = $request->all();

        $ttl = intval(env('CACHE_EXPIRATION', 300));

        $all_keys = json_decode(Cache::get('keys'));

        if(!is_array($all_keys)){
            $all_keys = [];
        }

        $old = 0;
        $new = 0;

        foreach ($vars as $key => $value)
        {
            if(Cache::has($key) == false && in_array($key, $all_keys) == false)
            {
                array_push($all_keys, $key);
                $new++;
            } else {
                $old++;
            }

            $added = Cache::put($key, $value, $ttl);
            
        }

        Cache::put('keys', json_encode($all_keys));

        return response()->json([
            'success' => true,
            'message' => 'Updated '. $old .' value(s) and stored '. $new .' value(s) successfully',
        ], 200);
    }

}
